
rxjh 根据配置，自动生成写ui代码的基础部分
2018.6.11 21:26


环境搭建：
1.修改mklink.bat,将其中的artres目录替换为你本地的artres路径
2.双击运行mklink.bat


生成代码：
1.先将要用到的控件命名
2.配置src/config.lua

```lua
config.cfg = 
{
	{layout = "tuding", name = "tuding", },
	{layout = "ly", name = "longyin" },
	{layout = "anqi", name = "anqi", scroll = { ["scroll1"]= "anqilbt", } },
}
```
* layout字段为layer下的布局文件名字
* name为要生成ui的lua文件名字
* scroll是一个list，里面保存了对应的scroll控件中，需要添加的widgets/控件名字

3. 双击run.bat 运行，生成的最终文件在genWndFiles目录中
4. have fun