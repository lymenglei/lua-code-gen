
config = require("src/config")
gen = require("src/gen")
pa = require("src/pa")
file = require("src/file")


srcFileDir = "src/artRes_layers" -- 原始目录
widgetsDir = "src/artRes_widgets"
genFileDir = "temp" -- 生成修改了布局的目录
genWndDir = "genWndFiles" -- 生成文件的最终目录


local function main()
	--[[
		1. 读取配置文件，根据已经命名的控件，生成对应的ui代码
	]]
	
	local cfg = config.cfg
	
	file.mkdir(genFileDir)
	file.mkdir(genWndDir)
	
	for k, v in ipairs(cfg) do
		local inputPath = srcFileDir.."/"..v.layout..".lua"
		local outputPath = genFileDir.."/"..v.layout..".lua"
		file.parse(inputPath, outputPath)
		
		-- 引用到的控件
		if v.scroll then
			for p, q in pairs(v.scroll) do
				local inputPath = widgetsDir.."/"..q..".lua"
				local outputPath = genFileDir.."/"..q..".lua"
				file.parse(inputPath, outputPath)
			end
		end
	end
	
	
	for k, v in ipairs(cfg) do
		local t = require(genFileDir.."/"..v.layout)
		local list = pa.parase(t)
		gen.genCode(v, list, genWndDir)
	end
	
	print("done")
end
main()
