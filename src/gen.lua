


local gen = {}

function gen.genWndName(cfg)
	return "wnd_"..cfg.name
end

local TYPE_SCROLL = "Scroll"
local TYPE_IMAGE  = "Image"
local TYPE_BUTTON = "Button"
local TYPE_LABEL  = "Label"

--------------------------------
-- gen function context		
-- tableNum: numbers of "\t" 
function handleLabelContent(tableNum, name, widgetName)
	local tabStr = ""
	for i = 1, tableNum do
		tabStr = tabStr .. "\t"
	end
	return tabStr..name.."."..widgetName..":setText()\n"
end

function handleImageContent(tableNum, name, widgetName)
	local tabStr = ""
	for i = 1, tableNum do
		tabStr = tabStr .. "\t"
	end
	return tabStr..name.."."..widgetName..":setImage()\n"
end

function handleButtonContent(tableNum, name, widgetName)
	local tabStr = ""
	for i = 1, tableNum do
		tabStr = tabStr .. "\t"
	end
	return tabStr..name.."."..widgetName..":onClick()\n"
end

function handleLabel(cfg, list)
	local wndName = gen.genWndName(cfg)
	local str = "function "..wndName..":setLabels()\n"..
	"\tlocal widgets = self._layout.vars\n"
	for k, v in ipairs(list) do
		str = str .. "\t-- "..handleLabelContent(1, "widgets", v)
	end
	str = str .. "end\n\n"
	return str
end

function handleImage(cfg, list)
	local wndName = gen.genWndName(cfg)
	local str = "function "..wndName..":setImages()\n"..
	"\tlocal widgets = self._layout.vars\n"
	for k, v in ipairs(list) do
		str = str .."\t-- "..handleImageContent(1, "widgets", v)
	end
	str = str .. "end\n\n"
	return str
end


function handleScrollImage(cfg, info)
	return handleImageContent(2, "ui.vars", info.varName)
end

function handleScrollLabel(cfg, info)
	return handleLabelContent(2, "ui.vars", info.varName)
end

function handleScrollButton(cfg, info)
	return handleButtonContent(2, "ui.vars", info.varName)
end

local scrollWidgets = 
{
--	[TYPE_SCROLL] = {action = handleScroll},
	[TYPE_IMAGE]  = {action = handleScrollImage},
	[TYPE_BUTTON] = {action = handleScrollButton},
	[TYPE_LABEL]  = {action = handleScrollLabel},
}

function gen.genScrollContent(cfg, scrollName, widget)
	local wndName = gen.genWndName(cfg)
	local str = "\nfunction "..wndName..":setScroll_"..scrollName.."(list)\n"..
	"\tlocal widgets = self._layout.vars\n"..
	"\tlocal scroll = widgets."..scrollName.."\n"..
	"\tscroll:removeAllChildren()\n"..
	"\tfor k, v in ipairs(list) do\n"..
	"\t\tlocal ui = require(\"ui/widgets/"..widget.."\")()\n"
	local t = require(genFileDir.."/"..widget)
	local list = pa.parase(t)
--	for k, v in ipairs(list) do
--		print(v.varName .. " ".. v.etype)
--	end
	
	local widgetStr = ""
	for k, v in ipairs(list) do 
		local conf = scrollWidgets[v.etype]
		if conf then -- 只解析已经注册到表中的结构
			local act = conf.action
			if act then
				widgetStr = widgetStr .. act(cfg, v)
			end
		end
	end
	str = str .. widgetStr
	
	str = str .."\tend\n"..
	"end\n\n"
	
	return str
end

function handleScroll(cfg, list)
	local wndName = gen.genWndName(cfg)
	local str = "function "..wndName..":setScrolls()\n"..
	"\tlocal widgets = self._layout.vars\n"
	for k, v in ipairs(list) do
		str = str .. "\t-- self:setScroll_"..v.."( {} )\n"
	end
	str = str .. "end\n\n"
	
	for k, v in ipairs(list) do
		if cfg.scroll and cfg.scroll[v] then
			local widget = cfg.scroll[v]
			str = str .. gen.genScrollContent(cfg, v, widget)
		end
	end
	return str
end

function gen.genButtonSender(cfg, list)
	local wndName = gen.genWndName(cfg)
	local res = ""
	for k, v in ipairs(list) do
		local str = "function "..wndName..":on"..v.."Btn(sender)\n"..
		"\t\n"..
		"end\n\n"
		res = res..str
	end
	return res
end

function handleButton(cfg, list)
	local wndName = gen.genWndName(cfg)
	local str = "function "..wndName..":setButtons()\n"..
	"\tlocal widgets = self._layout.vars\n"
	for k, v in ipairs(list) do
		str = str .. "\t-- widgets."..v..":onClick(self, self.on"..v.."Btn)\n"
	end
	str = str .. "end\n\n"
	local senders = gen.genButtonSender(cfg, list)
	return str..senders
end

local widgets = 
{
	[TYPE_SCROLL] = {action = handleScroll},
	[TYPE_IMAGE]  = {action = handleImage},
	[TYPE_BUTTON] = {action = handleButton},
	[TYPE_LABEL]  = {action = handleLabel},
}

-----------------------------------------------

local function clearFile(fileName)
	local file = io.open(fileName, "w") 
	io.output(file) 
	io.write("")
	io.close(file)
end

local function write(fileName, contextString)
	clearFile(fileName)
	
	local file = io.open(fileName, "a") -- 以附加的方式打开只写文件
	io.output(file) 
	io.write(contextString)
	io.close(file)
end 

-----------------------------------------------

function gen.genWndName(cfg)
	return "wnd_"..cfg.name
end

-- args is a list of string
function gen.genFunctionBody(cfg, funcName, context, args)
	context = context or "\t-- your codes here."
	local wndName = gen.genWndName(cfg)
	local argStr = ""
	if args then
		for k, v in ipairs(args) do
			argStr = argStr .. v
		end
	end
	local str = "function "..wndName..":"..funcName.."("..argStr..")\n"..
	context .."\n"..
	"end\n\n"
	return str
end

-------------------------------
function gen.genCtor(cfg)
	local funcName = "ctor"
	return gen.genFunctionBody(cfg, funcName, nil)
end

function gen.genConfigure(cfg)
	local funcName = "configure"
	return gen.genFunctionBody(cfg, funcName, nil)
end

function gen.genRefresh(cfg)
	local funcName = "refresh"
	return gen.genFunctionBody(cfg, funcName, nil)
end

function gen.genShowHide(cfg)
	local funcName = "show"
	local show =  gen.genFunctionBody(cfg, funcName, nil)
	
	local funcName = "hide"
	local hide =  gen.genFunctionBody(cfg, funcName, nil)
	return show..hide
end

function gen.genOnUpdate(cfg)
	local funcName = "onUpdate"
	local args = {"dTime"}
	return gen.genFunctionBody(cfg, funcName, nil, args)
end



function gen.genHeader(cfg)
	local str = 
	"-------------------------------------------------------\n"..
	"module(..., package.seeall)\n"..
	"local require = require;\n"..
	"local ui = require(\"ui/base\");\n"..
	"-------------------------------------------------------\n\n"

	return str
end

function gen.genFooter(cfg)
	local wndName = gen.genWndName(cfg)
	local str = 
	"function wnd_create(layout, ...)\n"..
	"\tlocal wnd = "..wndName..".new()\n"..
	"\twnd:create(layout, ...)\n"..
	"\treturn wnd;\n"..
	"end\n"
	return str
end

function gen.genWidgets(cfg, list)
	local t = {}
	
	for k, v in ipairs(list) do
		if not t[v.etype] then
			t[v.etype] = {}
		end
		table.insert(t[v.etype], v.varName)
	end
	
	local str = ""
	for k, v in pairs(t) do -- 相同类型的控件做统一的处理
		local conf = widgets[k]
		if conf then -- 只解析已经注册到表中的结构
			local act = conf.action
			str = str .. act(cfg, v)
		end
	end
	return str
end


function gen.genCode(cfg, list, genWndDir)
	local strList = {}
	local header = gen.genHeader(cfg)
	local ctor = gen.genCtor(cfg)
	local conf = gen.genConfigure(cfg)
	local refresh = gen.genRefresh(cfg)
	local showHide = gen.genShowHide(cfg)
	local onUpdate = gen.genOnUpdate(cfg)
	
	local func = gen.genWidgets(cfg, list)
	local footer = gen.genFooter(cfg)
	
	-------------------------------------
	--下面的顺序决定了生成代码函数的顺序
	table.insert(strList, header)
	
	table.insert(strList, ctor)
	table.insert(strList, conf)
	table.insert(strList, refresh)
	table.insert(strList, onUpdate)
	table.insert(strList, showHide)
	table.insert(strList, func)
	
	-- TODO add your code here
	
	table.insert(strList, footer)
	-------------------------------------
	local str = ""
	for k, v in ipairs(strList) do
		str = str..v
	end
	write(genWndDir.."/"..cfg.name..".lua", str)
end

return gen