
local file = {}

-- 判断目录或者文件是否存在
function file.exists(path)
	local file = io.open(path, "rb")
  	if file then 
  		file:close() 
  	end
  	return file ~= nil
end

function file.mkdir(fileName)
	os.execute("mkdir "..fileName)
end

function file.parse(inputFile, outputFile)
	local filePath = inputFile --"src/layers/ltzbt.lua"
	local file = io.open(filePath, "r")
	assert(file ~= nil, "\n\ninput file is a nil value : "..inputFile.."\n")
	local context = file:read("*a")
	io.close(file)
	
	context = string.gsub(context, "local l_fileType.*elements start tag", "")
	context = string.gsub(context, "end tag.*create", "")
	
	context = context.."return eleRoot"
	
	local file = io.open(outputFile, "w+")
	io.output(file)
	io.write(context)
	assert(file ~= nil, "\n\noutput file is a nil value : "..outputFile.."\n")
	io.close(file)
end


function file.test()
	local filePath = "src/layers/ltzbt.lua"
	local file = io.open(filePath, "r")
	local context = file:read("*a")
	io.close(file)
	
	context = string.gsub(context, "local l_fileType.*elements start tag", "")
	context = string.gsub(context, "end tag.*create", "")
	
	context = context.."return eleRoot"
	print(context)
	
	local file = io.open("src/layers/ltzbt2222.lua", "w+")
	io.output(file)
	io.write(context)
	io.close(file)
end


--file.test()

return file