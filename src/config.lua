

local config = {}


local TYPE_SCROLL = "Scroll"
local TYPE_IMAGE  = "Image"
local TYPE_BUTTON = "Button"
local TYPE_LABEL  = "Label"


config.widgets = 
{
	[TYPE_SCROLL] = {action = nil},
	[TYPE_IMAGE]  = {action = nil},
	[TYPE_BUTTON] = {action = nil},
	[TYPE_LABEL]  = {action = nil},
}


config.cfg = 
{
	{layout = "tuding", name = "tuding", },
	{layout = "ly", name = "longyin" },
	{layout = "anqi", name = "anqi", scroll = { ["scroll1"]= "anqilbt", } },
}

return config