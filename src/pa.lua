
local pa = {}


function getMapNums(t)
	local count = 0
	for k, v in pairs(t) do
		count = count + 1
	end
	return count
end


local list = {}

function pa.clean()
	list = {}
end

function pa.init(t)
	if t.prop then
		local varName = t.prop.varName
		local etype = t.prop.etype
		local text = t.prop.text
		if varName then
			table.insert(list, {varName = varName, etype = etype, text = text or "", })
		end
	end
	if t.children then
		for k, v in ipairs(t.children) do
			pa.init(v)
		end
	end
end

function pa.getList()
	return list
end

function pa.parase(t)
	pa.clean()
	pa.init(t)
	return pa.getList()
end


return pa